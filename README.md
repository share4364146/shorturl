# shorturl
Данный код нельзя применить в работe сразу же, но его будет достаточно для тестового

# Использование компонента в разделах:
Вы можете использовать компонент в любом месте, где вам нужно сократить URL:

<?php
$APPLICATION->IncludeComponent(
    'shorturl:shorturl',
    '.default',
    array('URL' => 'http://your-long-url.ru'),
    false
);

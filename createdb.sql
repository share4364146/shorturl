CREATE TABLE IF NOT EXISTS `long_urls` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `url` varchar(255) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `url` (`url`)
);

CREATE TABLE IF NOT EXISTS `short_codes` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `code` varchar(10) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `code` (`code`)
);

CREATE TABLE IF NOT EXISTS `url_mapping` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `long_url_id` int(11) NOT NULL,
    `short_code_id` int(11) NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`long_url_id`) REFERENCES `long_urls`(`id`),
    FOREIGN KEY (`short_code_id`) REFERENCES `short_codes`(`id`)
);

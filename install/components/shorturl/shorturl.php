<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class ShortURLComponent extends CBitrixComponent
{
    public function executeComponent()
    {
        $this->arResult['SHORT_URL'] = $this->shortenURL($this->arParams['URL']);
        $this->includeComponentTemplate();
    }

    private function shortenURL($url)
{
    $longUrlId = $this->getLongUrlId($url);

    if (!$longUrlId) {
        $longUrlId = $this->addLongUrl($url);
    }

    $shortCode = $this->generateShortCode();

    $shortCodeId = $this->addShortCode($shortCode);

    $this->addUrlMapping($longUrlId, $shortCodeId);

    return 'http://yourdomain.ru/' . $shortCode;
}

private function getLongUrlId($url)
{
    $connection = // (например, Bitrix\Main\Application::getConnection())

    $query = "
        SELECT id
        FROM long_urls
        WHERE url = '" . $connection->getSqlHelper()->forSql($url) . "'
    ";

    $result = $connection->query($query);
    $row = $result->fetch();

    return $row ? $row['id'] : false;
}

private function addLongUrl($url)
{
    $connection = // подключения к базе данных

    $query = "
        INSERT INTO long_urls (url)
        VALUES ('" . $connection->getSqlHelper()->forSql($url) . "')
    ";

    $connection->queryExecute($query);

    return $connection->getInsertedId();
}

private function generateShortCode()
{
    return substr(md5(uniqid()), 0, 6);
}

private function addShortCode($code)
{
    $connection = // подключения к базе данных

    $query = "
        INSERT INTO short_codes (code)
        VALUES ('" . $connection->getSqlHelper()->forSql($code) . "')
    ";

    $connection->queryExecute($query);

    return $connection->getInsertedId();
}

private function addUrlMapping($longUrlId, $shortCodeId)
{
    $connection = // подключения к базе данных

    $query = "
        INSERT INTO url_mapping (long_url_id, short_code_id)
        VALUES ($longUrlId, $shortCodeId)
    ";

    $connection->queryExecute($query);
}

}

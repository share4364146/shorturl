<?php
if (!check_bitrix_sessid()) return;

global $APPLICATION;

if ($_REQUEST["step"] < 2) {
    $APPLICATION->IncludeAdminFile('DeinstallModuleStep1', __DIR__.'/unstep1.php');
} elseif ($_REQUEST["step"] == 2) {    
    // отключить модуль:
    CModule::IncludeModule('shorturl');
    UnRegisterModule('shorturl');    

    // Удаление таблиц:
    global $DB;
    $DB->Query("DROP TABLE IF EXISTS long_urls");
    $DB->Query("DROP TABLE IF EXISTS short_codes");
    $DB->Query("DROP TABLE IF EXISTS url_mapping");
   
    // unlink($_SERVER["DOCUMENT_ROOT"]."/bitrix/components/shorturl/");
    
    $APPLICATION->IncludeAdminFile('DeinstallModuleStep2', __DIR__.'/unstep2.php');
}

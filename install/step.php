<?php
if (!check_bitrix_sessid()) return;

echo CAdminMessage::ShowMessage(array(
    'TYPE' => 'OK',
    'MESSAGE' => 'Модуль успешно установлен',
    'DETAILS' => 'Модуль "Short URL" успешно установлен',
    'HTML' => true,
));

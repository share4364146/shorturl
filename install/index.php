<?php
use Bitrix\Main\Loader;
use Bitrix\Main\EventManager;

class shorturl extends CModule
{
    var $MODULE_ID = 'shorturl';

    function __construct()
    {
        $arModuleVersion = array();
        include(__DIR__.'/version.php');

        $this->MODULE_ID = 'shorturl';
        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];

        $this->MODULE_NAME = 'Short URL';
        $this->MODULE_DESCRIPTION = 'Module for URL shortening in Bitrix24';
    }

    function DoInstall()
    {
        global $APPLICATION;
        RegisterModule($this->MODULE_ID);
        $APPLICATION->IncludeAdminFile('InstallModule', __DIR__.'/step.php');
    }

    function DoUninstall()
    {
        global $APPLICATION;
        UnRegisterModule($this->MODULE_ID);
        $APPLICATION->IncludeAdminFile('UninstallModule', __DIR__.'/unstep.php');
    }
}
